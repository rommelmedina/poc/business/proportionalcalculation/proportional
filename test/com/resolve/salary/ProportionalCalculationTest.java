/**
 * Copyright (c) 2021. Resuelve.
 * All Rights Reserved.
 * NOTICE:  All information contained herein is, and remains
 * the property of Resuelve and its suppliers, if any.
 * The intellectual and technical concepts contained
 * herein are proprietary to Resuelve and its suppliers and
 * may be covered by U.S. and Foreign Patents, patents in process,
 * and are protected by trade secret or copyright law.
 * Dissemination of this information or reproduction of this material
 * is strictly forbidden unless prior written permission is obtained
 * from Resuelve.
 */
package com.resolve.salary;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

/**
 * @author Rommel Medina
 *
 */
class ProportionalCalculationTest {
	
	public static final String PARAM_INVALID_BONUS_PERCENTAGE = "Incorrect bonus percentages.";
	public static final String PARAM_INVALID_ZERO_PRODUCTION_GOALS = "Goals production can't by zero.";

	@Test
	void test() {
		Set<String> goalProductionCategories = new HashSet<String>();
		goalProductionCategories.add("level");
		goalProductionCategories.add("group");
		
		HashMap<String, Double> productionGoalsZero = new HashMap<String, Double>();
		productionGoalsZero.put("level", Double.valueOf(0));
		productionGoalsZero.put("group", Double.valueOf(0));
		HashMap<String, Double> bonusPercentagesZero = new HashMap<String, Double>();
		bonusPercentagesZero.put("level", Double.valueOf(0));
		bonusPercentagesZero.put("group", Double.valueOf(0));
		HashMap<String, Double> productionQuantityZero = new HashMap<String, Double>();
		productionQuantityZero.put("level", Double.valueOf(0));
		productionQuantityZero.put("group", Double.valueOf(0));
		
		HashMap<String, Double> productionGoals = new HashMap<String, Double>();
		productionGoals.put("level", Double.valueOf(20));
		productionGoals.put("group", Double.valueOf(50));
		HashMap<String, Double> bonusPercentages = new HashMap<String, Double>();
		bonusPercentages.put("level", Double.valueOf(0.5));
		bonusPercentages.put("group", Double.valueOf(0.5));
		
		HashMap<String, Double> bonusPercentagesIncorrect = new HashMap<String, Double>();
		bonusPercentagesIncorrect.put("level", Double.valueOf(0.876987));
		bonusPercentagesIncorrect.put("group", Double.valueOf(0.832432));
		
		HashMap<String, Double> bonusPercentagesLong = new HashMap<String, Double>();
		bonusPercentagesLong.put("level", Double.valueOf(0.33333333333));
		bonusPercentagesLong.put("group", Double.valueOf(0.66666666667));
		
		HashMap<String, Double> bonusPercentages7030 = new HashMap<String, Double>();
		bonusPercentages7030.put("level", Double.valueOf(0.7));
		bonusPercentages7030.put("group", Double.valueOf(0.3));
		
		HashMap<String, Double> luisProductionQuantity = new HashMap<String, Double>();
		luisProductionQuantity.put("level", Double.valueOf(19));
		luisProductionQuantity.put("group", Double.valueOf(48));
		
		Exception exception = null;
		
		exception = assertThrows(IllegalArgumentException.class, ()-> calculate(goalProductionCategories,productionGoalsZero,bonusPercentagesZero,productionQuantityZero,0,0));
		assertEquals(exception.getMessage(), PARAM_INVALID_ZERO_PRODUCTION_GOALS);
		exception = assertThrows(IllegalArgumentException.class, ()-> calculate(goalProductionCategories,productionGoalsZero,bonusPercentagesZero,productionQuantityZero,50000,10000));
		assertEquals(exception.getMessage(), PARAM_INVALID_ZERO_PRODUCTION_GOALS);
		exception = assertThrows(IllegalArgumentException.class, ()-> calculate(goalProductionCategories,productionGoalsZero,bonusPercentages,productionQuantityZero,0,0));
		assertEquals(exception.getMessage(), PARAM_INVALID_ZERO_PRODUCTION_GOALS);
		exception = assertThrows(IllegalArgumentException.class, ()-> calculate(goalProductionCategories,productionGoalsZero,bonusPercentages,productionQuantityZero,50000,10000));
		assertEquals(exception.getMessage(), PARAM_INVALID_ZERO_PRODUCTION_GOALS);
		exception = assertThrows(IllegalArgumentException.class, ()-> calculate(goalProductionCategories,productionGoalsZero,bonusPercentages,luisProductionQuantity,0,0));
		assertEquals(exception.getMessage(), PARAM_INVALID_ZERO_PRODUCTION_GOALS);
		exception = assertThrows(IllegalArgumentException.class, ()-> calculate(goalProductionCategories,productionGoalsZero,bonusPercentages,luisProductionQuantity,50000,10000));
		assertEquals(exception.getMessage(), PARAM_INVALID_ZERO_PRODUCTION_GOALS);
		exception = assertThrows(IllegalArgumentException.class, ()-> calculate(goalProductionCategories,productionGoalsZero,bonusPercentagesIncorrect,productionQuantityZero,0,0));
		assertEquals(exception.getMessage(), PARAM_INVALID_ZERO_PRODUCTION_GOALS);
		
		exception = assertThrows(IllegalArgumentException.class, ()-> calculate(goalProductionCategories,productionGoals,bonusPercentagesIncorrect,luisProductionQuantity,0,0));
		assertEquals(exception.getMessage(), PARAM_INVALID_BONUS_PERCENTAGE);
		exception = assertThrows(IllegalArgumentException.class, ()-> calculate(goalProductionCategories,productionGoals,bonusPercentagesIncorrect,luisProductionQuantity,50000,10000));
		assertEquals(exception.getMessage(), PARAM_INVALID_BONUS_PERCENTAGE);
		
		assertEquals(0, calculate(goalProductionCategories,productionGoals,bonusPercentagesLong,luisProductionQuantity, 0, 0));
		assertEquals(0, calculate(goalProductionCategories,productionGoals,bonusPercentages,luisProductionQuantity, 0, 0));
		
		// Resuelve FC
		// luis Level Cuahu
		assertEquals(59550.00, calculate(goalProductionCategories,productionGoals,bonusPercentages,luisProductionQuantity, 50000, 10000));
		
		assertEquals(59533.33, calculate(goalProductionCategories,productionGoals,bonusPercentagesLong,luisProductionQuantity, 50000, 10000));
		assertEquals(59570.00, calculate(goalProductionCategories,productionGoals,bonusPercentages7030,luisProductionQuantity, 50000, 10000));
		
		// martin Level C
		HashMap<String, Double> productionQuantity = new HashMap<String, Double>();
		productionQuantity.put("level", Double.valueOf(16));
		productionQuantity.put("group", Double.valueOf(48));
		productionGoals.put("level", Double.valueOf(15));
		assertEquals(48820.0, calculate(goalProductionCategories,productionGoals,bonusPercentages,productionQuantity, 40000, 9000));
		// pedro Level B
		productionQuantity.put("level", Double.valueOf(7));
		productionGoals.put("level", Double.valueOf(10));
		assertEquals(36640.0, calculate(goalProductionCategories,productionGoals,bonusPercentages,productionQuantity, 30000, 8000));
		// juan Level A
		productionQuantity.put("level", Double.valueOf(6));
		productionGoals.put("level", Double.valueOf(5));
		assertEquals(26860.0, calculate(goalProductionCategories,productionGoals,bonusPercentages,productionQuantity, 20000, 7000));
		
		// Rojo
		//juanperez
		productionQuantity.put("level", Double.valueOf(10));
		productionQuantity.put("group", Double.valueOf(19));
		productionGoals.put("level", Double.valueOf(15));
		assertEquals(63083.33, calculate(goalProductionCategories,productionGoals,bonusPercentages,productionQuantity, 50000, 25000));
		//elrulo
		productionQuantity.put("level", Double.valueOf(9));
		productionGoals.put("level", Double.valueOf(10));
		assertEquals(39600.00, calculate(goalProductionCategories,productionGoals,bonusPercentages,productionQuantity, 30000, 15000));
		
		// Azul
		//elcuahu
		productionQuantity.put("level", Double.valueOf(30));
		productionQuantity.put("group", Double.valueOf(37));
		productionGoals.put("level", Double.valueOf(20));
		assertEquals(126100.0, calculate(goalProductionCategories,productionGoals,bonusPercentages,productionQuantity, 100000, 30000));
		//cosmefulanito
		productionQuantity.put("level", Double.valueOf(7));
		productionGoals.put("level", Double.valueOf(5));
		assertEquals(43050.0, calculate(goalProductionCategories,productionGoals,bonusPercentages,productionQuantity, 30000, 15000));
	}
	
	@Test
	void testStructure() {
		// Resuelve FC
		TeamDTO teamRfc = new TeamDTO();
		teamRfc.setId("ResuelveFC");
		teamRfc.setName(teamRfc.getId());
		// luis Level Cuahu
		teamRfc.addPlayer("Luis", "Cuahu", "ResuelveFC", 50000.0, 10000.0, 19.0);
		// martin Level C
		teamRfc.addPlayer("Martin", "C", "ResuelveFC", 40000.0, 9000.0, 16.0);
		// pedro Level B
		teamRfc.addPlayer("Pedro", "B", "ResuelveFC", 30000.0, 8000.0, 7.0);
		// juan Level A
		teamRfc.addPlayer("Juan", "A", "ResuelveFC", 20000.0, 7000.0, 6.0);
		teamRfc.setProduction(48.0);
		
		// Rojo
		TeamDTO teamRojo = new TeamDTO();
		teamRojo.setId("Rojo");
		teamRojo.setName(teamRojo.getId());
		//juanperez
		teamRojo.addPlayer("Juan Perez", "C", "Rojo", 50000.0, 25000.0, 10.0);
		//elrulo
		teamRojo.addPlayer("El Rulo", "B", "Rojo", 30000.0, 15000.0, 9.0);
		teamRojo.setProduction(19.0);
		
		// Azul
		TeamDTO teamAzul = new TeamDTO();
		teamAzul.setId("Azul");
		teamAzul.setName(teamAzul.getId());
		//elcuahu
		teamAzul.addPlayer("El Cuahu", "Cuahu", "Azul", 100000.0, 30000.0, 30.0);
		//cosmefulanito
		teamAzul.addPlayer("Cosme Fulanito", "A", "Azul", 30000.0, 10000.0, 7.0);
		teamAzul.setProduction(37.0);
		
		ProportionalCalculationAdapter pca = new ProportionalCalculationAdapter();
		
		pca.compute(teamRfc);
		pca.compute(teamRojo);
		pca.compute(teamAzul);
		
		// Resuelve FC
		// luis Level Cuahu
		assertEquals(59550.00, teamRfc.getPlayer("Luis").getFullSalary());
		// martin Level C
		assertEquals(48820.0, teamRfc.getPlayer("Martin").getFullSalary());
		// pedro Level B
		assertEquals(36640.0, teamRfc.getPlayer("Pedro").getFullSalary());
		// juan Level A
		assertEquals(26860.0, teamRfc.getPlayer("Juan").getFullSalary());
		
		// Rojo
		//juanperez
		assertEquals(70833.33, teamRojo.getPlayer("Juan Perez").getFullSalary());
		//elrulo
		assertEquals(44250.0, teamRojo.getPlayer("El Rulo").getFullSalary());
		
		// Azul
		//elcuahu
		assertEquals(130000.0, teamAzul.getPlayer("El Cuahu").getFullSalary());
		//cosmefulanito
		assertEquals(40000.0, teamAzul.getPlayer("Cosme Fulanito").getFullSalary());
		
	}

	double calculate(
			Set<String> goalProductionCategories,
			HashMap<String, Double> productionGoals,
			HashMap<String, Double> bonusPercentages,
			HashMap<String, Double> productionQuantity,
			double fixedSalaryAmount,
			double bonusAmount) {
		double salary = 0;
		
		validateproductionGoals(productionGoals);
		validateBonusPercentages(new ArrayList<Double>(bonusPercentages.values()));
		
		cutOverProduction(
				goalProductionCategories,
				productionGoals,
				productionQuantity);
		
		HashMap<String, Double> achievedGoalPercentages = getAchievedGoalPercentages(
				goalProductionCategories,
				productionGoals,
				productionQuantity);
		
		List<Double> achievedBonusPercentages = getBonusPercentagesFromProductionPortion(
				goalProductionCategories,
				bonusPercentages,
				achievedGoalPercentages);
		
		double bonus = getBonus(bonusAmount, achievedBonusPercentages);
		
		salary = fixedSalaryAmount + bonus;
		
		BigDecimal bd = new BigDecimal(salary).setScale(2, RoundingMode.HALF_DOWN);
		return bd.doubleValue();
	}
	
	double getBonus(
			double bonusAmount,
			List<Double> achievedBonusPercentages) {
		double bonus = 0;
		double sum = achievedBonusPercentages.stream().mapToDouble(Double::doubleValue).sum();
		bonus = bonusAmount * (sum);
		return bonus;
	}
	
	double getBonus(
			double bonusAmount,
			double achievedProportionalCategoryProduction,
			double achievedProportionalGroupProduction)
	{
		double bonus = bonusAmount * (achievedProportionalCategoryProduction + achievedProportionalGroupProduction);
		return bonus;
	}
	
	void cutOverProduction(
			Set<String> goalProductionCategories,
			HashMap<String, Double> productionGoals,
			HashMap<String, Double> productionQuantities) {
		for (String category : goalProductionCategories) {
			if (productionQuantities.get(category) > productionGoals.get(category)) {
				productionQuantities.put(category, productionGoals.get(category));
			}
		}
	}
	
	double cutOverProduction(double productionGoals, double production) {
		if (production > productionGoals) {
			production = productionGoals;
		}
		return production;
	}
	
	HashMap<String, Double> getAchievedGoalPercentages(
			Set<String> goalProductionCategories,
			HashMap<String, Double> productionGoals,
			HashMap<String, Double> productionQuantitys) {
		HashMap<String, Double> achieved = new HashMap<String, Double>();
		
		for (String category : goalProductionCategories) {
			Double achievedItem = productionQuantitys.get(category) / productionGoals.get(category);
			achieved.put(category, achievedItem);
		}
		
		return achieved;
	}
	
	List<Double> getBonusPercentagesFromProductionPortion(
			Set<String> goalProductionCategories,
			HashMap<String, Double> bonusPercentages,
			HashMap<String, Double> achievedGoalPercentages) {
		List<Double> achievedBonusPercentages = new ArrayList<Double>();
		
		for (String category : goalProductionCategories) {
			Double bonusPercentageItem = (1 - bonusPercentages.get(category)) * achievedGoalPercentages.get(category);
			achievedBonusPercentages.add(bonusPercentageItem);
		}
		
		return achievedBonusPercentages;
	}
	
	void validateBonusPercentages(List<Double> bonusPercentages) {
		double sum = bonusPercentages.stream().mapToDouble(Double::doubleValue).sum();
		if (sum != 1)
		{
			throw new IllegalArgumentException(PARAM_INVALID_BONUS_PERCENTAGE);
		}
	}
	
	void validateproductionGoals(HashMap<String, Double> productionGoals) {
		Collection<Double> goals = productionGoals.values();
		if (goals.contains(Double.valueOf(0))) {
			throw new IllegalArgumentException(PARAM_INVALID_ZERO_PRODUCTION_GOALS);
		}
	}
	
	/**
	 * DataTransferObject Player.
	 * @author Rommel Medina
	 *
	 */
	public class PlayerDTO{
		
		/**
		 * Unique id.
		 */
		private String id;
		
		/**
		 * Player name.
		 */
		private String name;
		
		/**
		 * Player level.
		 */
		private String level;
		
		/**
		 * Player team.
		 */
		private String team;
		
		/**
		 * Player fixed salary.
		 */
		private Double fixedSalary;
		
		/**
		 * Player bonus.
		 */
		private  Double bonus;
		
		/**
		 * Player final Salary.
		 */
		private Double fullSalary;
		
		/**
		 * Scored goals.
		 */
		private Double scoredPoints;
		
		/**
		 * Player's id.
		 * @return id Player's id.
		 */
		public String getId() {
			return id;
		}

		/**
		 * Set Player's id.
		 * @param id Player's id.
		 */
		public void setId(String id) {
			this.id = id;
		}

		/**
		 * Player name.
		 * @return name
		 */
		public String getName() {
			return name;
		}

		/**
		 * Player name.
		 * @param name the player's name.
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * Player's category level.
		 * @return level
		 */
		public String getLevel() {
			return level;
		}

		/**
		 * Set player's level.
		 * @param level player's level.
		 */
		public void setLevel(String level) {
			this.level = level;
		}

		/**
		 * Player's team.
		 * @return team Player's team name.
		 */
		public String getTeam() {
			return team;
		}

		/**
		 * Set player's team.
		 * @param team Team name.
		 */
		public void setTeam(String team) {
			this.team = team;
		}

		/**
		 * Player's salary base.
		 * @return fixedSalary Player's salary base.
		 */
		public Double getFixedSalary() {
			return fixedSalary;
		}

		/**
		 * Set player's salary base.
		 * @param fixedSalary Player's salary base.
		 */
		public void setFixedSalary(Double fixedSalary) {
			this.fixedSalary = fixedSalary;
		}

		/**
		 * Player's bonus amount.
		 * @return bonus Player's bonus amount. 
		 */
		public Double getBonus() {
			return bonus;
		}

		/**
		 * Set player's bonus amount.
		 * @param bonus Player's bonus amount.
		 */
		public void setBonus(Double bonus) {
			this.bonus = bonus;
		}
		
		/**
		 * Net earnings. Complete salary include all additional bonus.
		 * @return fullSalary Complete salary.
		 */
		public Double getFullSalary() {
			return fullSalary;
		}
		
		/**
		 * Set complete salary, including all additional bonus.
		 * @param fullSalary Complete salary.
		 */
		public void setFullSalary(Double fullSalary) {
			this.fullSalary = fullSalary;
		}

		/**
		 * Player's scored points.
		 * @return scoredPoints Player's scored points.
		 */
		public Double getScoredPoints() {
			return scoredPoints;
		}

		/**
		 * Set player's scored points.
		 * @param scoredPoints Player's scored points.
		 */
		public void setScoredPoints(Double scoredPoints) {
			this.scoredPoints = scoredPoints;
		}
	}
	
	/**
	 * Team DataTransferObject.
	 * @author Rommel Medina
	 *
	 */
	public class TeamDTO{
		/***
		 * Unique Id.
		 */
		private String id;
		
		/**
		 * Team name.
		 */
		private String name;
		
		/**
		 * Current players.
		 */
		private List<PlayerDTO> players;
		
		/**
		 * Team production.
		 */
		private Double production;

		/**
		 * Team's id.
		 * @return id Team's id. 
		 */
		public String getId() {
			return id;
		}

		/**
		 * Set Team's id.
		 * @param id Team's id.
		 */
		public void setId(String id) {
			this.id = id;
		}

		/**
		 * Team's name.
		 * @return name Team's name.
		 */
		public String getName() {
			return name;
		}

		/**
		 * Set Team's name.
		 * @param name Team's name.
		 */
		public void setName(String name) {
			this.name = name;
		}

		/**
		 * Team player's.
		 * @return players Team player's.
		 */
		public List<PlayerDTO> getPlayers() {
			return players;
		}

		/**
		 * Set Team player's.
		 * @param players Team player's list.
		 */
		public void setPlayers(List<PlayerDTO> players) {
			this.players = players;
		}

		/**
		 * Team's scored goals production.
		 * @return production Team's scored goals production.
		 */
		public Double getProduction() {
			return production;
		}

		/**
		 * Set Team's scored goals production.
		 * @param production Team's scored goals production.
		 */
		public void setProduction(Double production) {
			this.production = production;
		}
		
		/**
		 * Add new player.
		 * @param name
		 * @param level
		 * @param team
		 * @param fixedSalary
		 * @param bonus
		 * @param scoredPoints
		 */
		public void addPlayer(
				String name,
				String level,
				String team,
				Double fixedSalary,
				Double bonus,
				Double scoredPoints
				) {
			if (this.players == null) {
				this.players = new ArrayList<PlayerDTO>();
			}
			PlayerDTO player = new PlayerDTO();
			player.setId(name);
			player.setName(name);
			player.setLevel(level);
			player.setTeam(team);
			player.setFixedSalary(fixedSalary);
			player.setBonus(bonus);
			player.setScoredPoints(scoredPoints);
			this.players.add(player);
		}
		
		/**
		 * Search player by name.
		 * @param name Payer's name.
		 * @return player
		 */
		public PlayerDTO getPlayer(String name) {
			String searched = name.trim().toLowerCase();
			return this.players.stream()
					.filter(player -> searched.equals(player.name.trim().toLowerCase()))
					.findAny()
					.orElse(null);
		}
	}
	
	/**
	 * Bonus portions definition.
	 * @author Rommel Medina
	 *
	 */
	public class BonusPercentagesDefinition{
		
		/**
		 * Definition id.
		 */
		private String id;
		
		/**
		 * Proportional part for each bonus.
		 */
		private HashMap<String, Double> percentages;
		
		/**
		 * Id.
		 * @return id Id.
		 */
		public String getId() {
			return id;
		}

		/**
		 * Set Id.
		 * @param id Id.
		 */
		public void setId(String id) {
			this.id = id;
		}

		/**
		 * Bonus percentages.
		 * @return percentages Bonus percentages.
		 */
		public HashMap<String, Double> getPercentages() {
			return percentages;
		}

		/**
		 * Set Bonus percentages.
		 * @param percentages Bonus percentages.
		 */
		public void setPercentages(HashMap<String, Double> percentages) {
			this.percentages = percentages;
		}
		
		/**
		 * Add category percentage.
		 * @param category Category.
		 * @param percentage Percentage.
		 */
		public void addPercentages(String category, Double percentage) {
			if (this.percentages == null) {
				this.percentages = new HashMap<String, Double>();
			}
			this.percentages.put(category, percentage);
		}
	}
	
	/**
	 * Breakdown for specific category.
	 * @author Rommel Medina
	 *
	 */
	public class CategoryBreakdown{
		
		/**
		 * Definition id.
		 */
		private String id;
		
		/**
		 * Definition table with each category level.
		 */
		private HashMap<String, Double> data;

		/**
		 * Breakdown id.
		 * @return id Breakdown id.
		 */
		public String getId() {
			return id;
		}

		/**
		 * Set Breakdown id.
		 * @param id Breakdown id.
		 */
		public void setId(String id) {
			this.id = id;
		}

		/**
		 * Breakdown data.
		 * @return data Breakdown data.
		 */
		public HashMap<String, Double> getData() {
			return data;
		}
		
		/**
		 * Get value from specific category.
		 * @param category Finded category.
		 * @return value For category.
		 */
		public Double getValue(String category) {
			return data.get(category);
		}

		/**
		 * Set Breakdown data.
		 * @param data Breakdown data.
		 */
		public void setData(HashMap<String, Double> data) {
			this.data = data;
		}
		
		/**
		 * Add category definition data.
		 * @param category Category name.
		 * @param value Decimal value to this category.
		 */
		public void addData(String category, Double value) {
			if (this.data == null) {
				this.data = new HashMap<String, Double>();
			}
			this.data.put(category, value);
		}
		
		/**
		 * Search the category for a single value.
		 * @param value Category value.
		 * @return category A single value's category.
		 */
		public String getCategory(Double value) {
			String category = this.getLast();
			if (this.data != null && this.data.size() > 0) {
				for (Entry<String, Double> entry : this.data.entrySet()) { 
		            if (value <= entry.getValue()) {
		                category = entry.getKey();
		            }
		        }
			}
			
			return category;
		}
		
		/**
		 * Sort the category data by data values.
		 */
		public void sortData() {
			if (this.data != null && this.data.size() > 0) {
				HashMap<String, Double> sortedMap = 
						this.data.entrySet().stream()
					    .sorted(Entry.comparingByValue())
					    .collect(Collectors.toMap(Entry::getKey, Entry::getValue,
					                              (e1, e2) -> e1, LinkedHashMap::new));
				
				this.data = sortedMap;
			}
		}
		
		/**
		 * Get last item in breakdown.
		 * @return value Double data value.
		 */
		private String getLast() {
			String lastItem = null;
			if (this.data != null && this.data.size() > 0) {
				Iterator<String> iterator = this.data.keySet().iterator();
				while(iterator.hasNext()){
			        lastItem = iterator.next();
			    }
			}
			return lastItem;
		}
	}
	
	/**
	 * Categories definitions.
	 * @author Rommel Medina
	 *
	 */
	public class CategoriesDefinition{
		
		/**
		 * Definition id.
		 */
		private String id;
		
		/**
		 * Category names.
		 */
		private Set<String> categories;
		
		/**
		 * Breakdowns.
		 */
		private HashMap<String, CategoryBreakdown> breakdowns;

		/**
		 * Category definition id.
		 * @return id Category definition id.
		 */
		public String getId() {
			return id;
		}

		/**
		 * Set Category definition id. 
		 * @param id Category definition id.
		 */
		public void setId(String id) {
			this.id = id;
		}

		/**
		 * Category names.
		 * @return categories Category names.
		 */
		public Set<String> getCategories() {
			return categories;
		}
		
		/**
		 * Set Category names.
		 * @param categories Category names.
		 */
		public void setCategories(Set<String> categories) {
			this.categories = categories;
		}

		/**
		 * Add category name.
		 * @param category Name.
		 */
		public void addCategories(String category) {
			if (this.categories == null) {
				this.categories = new HashSet<String>();
			}
			this.categories.add(category);
		}

		/**
		 * Category breakdowns.
		 * @return breakdowns Category breakdowns.
		 */
		public HashMap<String, CategoryBreakdown> getBreakdowns() {
			return breakdowns;
		}

		/**
		 * Set Category breakdowns.
		 * @param breakdowns Category breakdowns.
		 */
		public void setBreakdowns(HashMap<String, CategoryBreakdown> breakdowns) {
			this.breakdowns = breakdowns;
			for (CategoryBreakdown breakdown : breakdowns.values()) {
				breakdown.sortData();
			}
		}
		
		/**
		 * Get specific breakdown by category.
		 * @param category Category name.
		 * @return breakdown Category breakdown.
		 */
		public CategoryBreakdown getBreakdown(String category) {
			return breakdowns.get(category);
		}

		/**
		 * Add category breakdown.
		 * @param category Category name.
		 * @param breakdown Category breakdown.
		 */
		public void addBreakdowns(String category, CategoryBreakdown breakdown) {
			if (this.breakdowns == null) {
				this.breakdowns = new HashMap<String, CategoryBreakdown>();
			}
			breakdown.sortData();
			this.breakdowns.put(category, breakdown);
		}
		
		/**
		 * Get data value for specific sub category.
		 * @param category Category name.
		 * @param subCategory Inner category name.
		 * @return value Data value.
		 */
		public Double getCategoryDefinition(String category, String subCategory) {
			return this.getBreakdown(category).data.get(subCategory);
		}
		
		public HashMap<String, Double> getSpecificCategoryDefinitions(HashMap<String, String> specificCategories){
			HashMap<String, Double> definition = new HashMap<String, Double>();
			for (Entry<String, String> entry : specificCategories.entrySet()) { 
				CategoryBreakdown cb = this.getBreakdown(entry.getKey());
				Double value = cb.getValue(entry.getValue());
	            definition.put(entry.getKey(), value);
	        }
			return definition;
		}
	}
	
	/**
	 * Proportional definition.
	 * @author Rommel Medina
	 *
	 */
	public class ProportionalDefinition{
		
		/**
		 * Definition id.
		 */
		private String id;
		
		/**
		 * Categories definition.
		 */
		private CategoriesDefinition categories;
		
		/**
		 * Proportional part for each bonus.
		 */
		private BonusPercentagesDefinition percentages;

		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public CategoriesDefinition getCategories() {
			return categories;
		}

		public void setCategories(CategoriesDefinition categories) {
			this.categories = categories;
		}

		public BonusPercentagesDefinition getPercentages() {
			return percentages;
		}

		public void setPercentages(BonusPercentagesDefinition percentages) {
			this.percentages = percentages;
		}
	}
	
	/**
	 * Single item for proportional calculation.
	 * @author Rommel Medina
	 *
	 */
	public class ProportionalCalculationItem{
		/**
		 * Player name.
		 */
		private String id;
		
		/**
		 * Fixed base.
		 */
		private Double fixedBase;
		
		/**
		 * Proportional bonus.
		 */
		private  Double bonus;
		
		/**
		 * Calculation result value.
		 */
		private Double result;
		
		/**
		 * Sub category to which it belongs in each category.
		 */
		private HashMap<String, String> category;
		
		/**
		 * Player generated production.
		 */
		private HashMap<String, Double> productionQuantity;
		
		public String getId() {
			return id;
		}

		public void setId(String id) {
			this.id = id;
		}

		public Double getFixedBase() {
			return fixedBase;
		}

		public void setFixedBase(Double fixedBase) {
			this.fixedBase = fixedBase;
		}

		public Double getBonus() {
			return bonus;
		}

		public void setBonus(Double bonus) {
			this.bonus = bonus;
		}

		public Double getResult() {
			return result;
		}

		public void setResult(Double result) {
			this.result = result;
		}
		
		public HashMap<String, String> getCategory() {
			return category;
		}

		public void setCategory(HashMap<String, String> category) {
			this.category = category;
		}
		
		public void addCategory(String category, String subCategory) {
			if (this.category == null) {
				this.category = new HashMap<String, String>();
			}
			this.category.put(category, subCategory);
		}

		public HashMap<String, Double> getProductionQuantity() {
			return productionQuantity;
		}

		public void setProductionQuantity(HashMap<String, Double> productionQuantity) {
			this.productionQuantity = productionQuantity;
		}
	}
	
	/**
	 * Data for proportional calculation.
	 * @author Rommel Medina
	 *
	 */
	public class ProportionalDefinitionProvider{
		
		/**
		 * Static provider implementation.
		 */
		private StaticProportionalDefinitionProvider spdp = new StaticProportionalDefinitionProvider();
		
		/**
		 * Retrieve proportional definition.
		 * @param definitionId Definition Id.
		 * @return
		 */
		public ProportionalDefinition get(String definitionId) {
			return this.spdp.get(definitionId);
		}
	}
	
	/**
	 * Data for proportional calculation.
	 * @author Rommel Medina
	 *
	 */
	public class ProportionalCalculationData{
		/**
		 * Definitions for proportional calculation.
		 */
		private ProportionalDefinition definition;
		
		/**
		 * Definition provider service.
		 */
		private ProportionalDefinitionProvider definitionProvider = new ProportionalDefinitionProvider();

		public ProportionalDefinition getDefinition(String definitionId) {
			this.definition = this.definitionProvider.get(definitionId);
			return definition;
		}

		public void setDefinition(ProportionalDefinition definition) {
			this.definition = definition;
		}
		
		public HashMap<String, Double> getCategoryDefinitions(ProportionalCalculationItem pci){
			return this.definition.getCategories().getSpecificCategoryDefinitions(pci.getCategory());
		}
	}
	
	/**
	 * Convert PlayerDTO to PorportionalCalculationItem.
	 * @author Rommel Medina
	 *
	 */
	public class ProportionalCalculationItemMapper{
		/**
		 * Convert player to proportional calculation item.
		 * @param player
		 * @return ProportionalCalculationItem
		 */
		public ProportionalCalculationItem Convert(PlayerDTO player) {
			ProportionalCalculationItem item = new ProportionalCalculationItem();
			item.id = player.id;
			item.fixedBase = player.fixedSalary;
			item.bonus = player.bonus;
			item.addCategory(SalaryConstants.CATEGORY_NAME_LEVEL, player.level.trim().toLowerCase());
			item.productionQuantity = new HashMap<String, Double>();
			item.productionQuantity.put(SalaryConstants.CATEGORY_NAME_LEVEL, player.getScoredPoints());
			
			return item;
		}
	}
	
	/**
	 * Fill Request DTO with results form Proportional Calculation.
	 * @author Rommel Medina
	 *
	 */
	public class ProportionalMapper{
		/**
		 * Specific mapper.
		 */
		private ProportionalCalculationItemMapper mapper = new ProportionalCalculationItemMapper();
		
		/**
		 * Convert TeamDTO into Proportional calculation items list.
		 * @param request
		 * @return
		 */
		public List<ProportionalCalculationItem> Convert(TeamDTO request) {
			List<ProportionalCalculationItem> items = new ArrayList<ProportionalCalculationItem>();
			
			for(PlayerDTO findedPlayer : request.players) {
				ProportionalCalculationItem pci = this.mapper.Convert(findedPlayer);
				
				if (pci != null) {
					pci.addCategory(SalaryConstants.CATEGORY_NAME_GROUP, request.name.trim().toLowerCase());
					pci.getProductionQuantity().put(SalaryConstants.CATEGORY_NAME_GROUP, request.production);
					items.add(pci);
				}
			}
			return items;
		}
	}
	
	/**
	 * Fill Request DTO with results form Proportional Calculation.
	 * @author Rommel Medina
	 *
	 */
	public class ProportionalResultMapper{
		
		/**
		 * Fill the TeamDTO request with ProportionalCalculaitons results.
		 * @param request Original TeamDto.
		 * @param results From proportional calculation data.
		 * @return TeamDTO
		 */
		public TeamDTO Convert(TeamDTO request, List<ProportionalCalculationItem> results) {
			for(PlayerDTO findedPlayer : request.players) {
				ProportionalCalculationItem pci = results.stream()
						.filter(player -> findedPlayer.id.equals(player.id))
						.findAny()
						.orElse(null);
				
				if (pci != null) {
					findedPlayer.setFullSalary(pci.result);
				}
			}
			return request;
		}
	}
	
	/**
	 * Proportional calculation service.
	 * @author Rommel Medina
	 *
	 */
	public class ProportionalCalculation{
		
		/**
		 * Calculation data.
		 */
		private ProportionalCalculationData data = new ProportionalCalculationData();
		
		/**
		 * Apply proportional calculation.
		 * @param items Input data.
		 * @param id Proportional calculation data Id.
		 * @return Items with calculation applied.
		 */
		public List<ProportionalCalculationItem> compute(String id, List<ProportionalCalculationItem> items) {
			for (ProportionalCalculationItem pci : items) {
				double pcResult = ProportionalCalculationTest.this.calculate(
						data.getDefinition(id).categories.categories,
						data.getCategoryDefinitions(pci),
						data.getDefinition(id).percentages.percentages,
						pci.getProductionQuantity(),
						pci.getFixedBase(),
						pci.getBonus());
				pci.setResult(pcResult);
			}
			return items;
		}
	}
	
	/**
	 * Communication channel to proportional calculation service.
	 * @author Rommel Medina
	 *
	 */
	public class ProportionalCalculationAdapter{
		/**
		 * Mapper for request.
		 */
		private ProportionalMapper requestMapper = new ProportionalMapper();
		
		/**
		 * Response mapper.
		 */
		private ProportionalResultMapper resultMapper = new ProportionalResultMapper();
		
		/**
		 * Proportional Calculation Service.
		 */
		private ProportionalCalculation service = new ProportionalCalculation();
		
		/**
		 * Apply proportional calculation.
		 * @param request Team
		 * @return Team with salary calculation.
		 */
		public TeamDTO compute(TeamDTO request) {
			List<ProportionalCalculationItem> input = this.requestMapper.Convert(request);
			
			List<ProportionalCalculationItem> results = this.service.compute(request.getId(), input);
			
			this.resultMapper.Convert(request, results);
			
			return request;
		}
	}
	
	public static class SalaryConstants{
		public final static String CATEGORY_NAME_LEVEL = "level";
		public final static String CATEGORY_NAME_GROUP = "group";
	}
	
	/**
	 * Build static dummy proportional definition.
	 * @author rommelmg
	 *
	 */
	public class StaticProportionalDefinitionProvider{
		
		/**
		 * Definitions Lists.
		 */
		private HashMap<String, ProportionalDefinition> defintions = new HashMap<String, ProportionalDefinition>();
		
		/**
		 * Get proportional definition.
		 * @param id Id for recover definition.
		 * @return proportionalDefinition.
		 */
		public ProportionalDefinition get(String id) {
			id = id.trim().toLowerCase();
			ProportionalDefinition pd = this.defintions.get(id);
			if (pd == null) {
				switch(id) {
				case "resuelvefc":
					pd = this.getResuelveFC();
					break;
				case "rojo":
					pd = this.getRojo();
					break;
				case "azul":
					pd = this.getAzul();
					break;
				default:
					break;
				}
				if (pd != null) {
					this.defintions.put(id, pd);
				}
			}
			return pd;
		}
		
		/**
		 * Retrieve proportional definition for ResuelveFC.
		 * @return
		 */
		public ProportionalDefinition getResuelveFC() {
			ProportionalDefinition definition = new ProportionalDefinition();
			definition.setId("resuelvefc");
			BonusPercentagesDefinition bonusDefinition = new BonusPercentagesDefinition();
			bonusDefinition.addPercentages(SalaryConstants.CATEGORY_NAME_LEVEL, 0.50);
			bonusDefinition.addPercentages(SalaryConstants.CATEGORY_NAME_GROUP, 0.50);
			
			definition.setPercentages(bonusDefinition);
			
			CategoryBreakdown singlePlayerGoals = new CategoryBreakdown();
			singlePlayerGoals.setId(SalaryConstants.CATEGORY_NAME_LEVEL);
			singlePlayerGoals.addData("a", 5.0);
			singlePlayerGoals.addData("b", 10.0);
			singlePlayerGoals.addData("c", 15.0);
			singlePlayerGoals.addData("cuahu", 20.0);
			
			CategoryBreakdown grouoPlayerGoals = new CategoryBreakdown();
			grouoPlayerGoals.setId(SalaryConstants.CATEGORY_NAME_GROUP);
			grouoPlayerGoals.addData("resuelvefc", 50.0);
			
			CategoriesDefinition catDefinition = new CategoriesDefinition();
			catDefinition.addCategories(singlePlayerGoals.getId());
			catDefinition.addCategories(grouoPlayerGoals.getId());
			catDefinition.addBreakdowns(singlePlayerGoals.getId(), singlePlayerGoals);
			catDefinition.addBreakdowns(grouoPlayerGoals.getId(), grouoPlayerGoals);
			
			definition.setCategories(catDefinition);
			
			return definition;
		}
		
		/**
		 * Retrieve proportional definition for Rojo.
		 * @return
		 */
		public ProportionalDefinition getRojo() {
			ProportionalDefinition definition = new ProportionalDefinition();
			definition.setId("rojo");
			BonusPercentagesDefinition bonusDefinition = new BonusPercentagesDefinition();
			bonusDefinition.addPercentages(SalaryConstants.CATEGORY_NAME_LEVEL, 0.50);
			bonusDefinition.addPercentages(SalaryConstants.CATEGORY_NAME_GROUP, 0.50);
			
			definition.setPercentages(bonusDefinition);
			
			CategoryBreakdown singlePlayerGoals = new CategoryBreakdown();
			singlePlayerGoals.setId(SalaryConstants.CATEGORY_NAME_LEVEL);
			singlePlayerGoals.addData("a", 5.0);
			singlePlayerGoals.addData("b", 10.0);
			singlePlayerGoals.addData("c", 15.0);
			singlePlayerGoals.addData("cuahu", 20.0);
			
			CategoryBreakdown grouoPlayerGoals = new CategoryBreakdown();
			grouoPlayerGoals.setId(SalaryConstants.CATEGORY_NAME_GROUP);
			grouoPlayerGoals.addData("rojo", 19.0);
			
			CategoriesDefinition catDefinition = new CategoriesDefinition();
			catDefinition.addCategories(singlePlayerGoals.getId());
			catDefinition.addCategories(grouoPlayerGoals.getId());
			catDefinition.addBreakdowns(singlePlayerGoals.getId(), singlePlayerGoals);
			catDefinition.addBreakdowns(grouoPlayerGoals.getId(), grouoPlayerGoals);
			
			definition.setCategories(catDefinition);
			
			return definition;
		}
		
		/**
		 * Retrieve proportional definition for Azul.
		 * @return
		 */
		public ProportionalDefinition getAzul() {
			ProportionalDefinition definition = new ProportionalDefinition();
			definition.setId("azul");
			BonusPercentagesDefinition bonusDefinition = new BonusPercentagesDefinition();
			bonusDefinition.addPercentages(SalaryConstants.CATEGORY_NAME_LEVEL, 0.50);
			bonusDefinition.addPercentages(SalaryConstants.CATEGORY_NAME_GROUP, 0.50);
			
			definition.setPercentages(bonusDefinition);
			
			CategoryBreakdown singlePlayerGoals = new CategoryBreakdown();
			singlePlayerGoals.setId(SalaryConstants.CATEGORY_NAME_LEVEL);
			singlePlayerGoals.addData("a", 5.0);
			singlePlayerGoals.addData("b", 10.0);
			singlePlayerGoals.addData("c", 15.0);
			singlePlayerGoals.addData("cuahu", 20.0);
			
			CategoryBreakdown grouoPlayerGoals = new CategoryBreakdown();
			grouoPlayerGoals.setId(SalaryConstants.CATEGORY_NAME_GROUP);
			grouoPlayerGoals.addData("azul", 37.0);
			
			CategoriesDefinition catDefinition = new CategoriesDefinition();
			catDefinition.addCategories(singlePlayerGoals.getId());
			catDefinition.addCategories(grouoPlayerGoals.getId());
			catDefinition.addBreakdowns(singlePlayerGoals.getId(), singlePlayerGoals);
			catDefinition.addBreakdowns(grouoPlayerGoals.getId(), grouoPlayerGoals);
			
			definition.setCategories(catDefinition);
			
			return definition;
		}
	}
}